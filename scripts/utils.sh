F_BOLD="\033[1m"
F_GREEN="\033[32m"
F_GRAY="\033[37m"
F_RESET="\033[0m"

function iterate() {
  local path=$1
  local consumer=$2
  for i in "$path"/*; do
    ${consumer} "$i"
  done
}

function filename_strip_extension() {
  local name
  name=$(filename "${1}")
  echo "${name%.*}"
}

function filename() {
  echo "${1##*/}"
}

function sha1() {
  local full
  full=$(sha1sum "$1" 2>/dev/null || shasum "$1")
  echo "${full%% *}"
}
